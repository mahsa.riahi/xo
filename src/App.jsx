import React , {useState , useEffect} from "react"
import styled from "styled-components";

const App = () =>{

    let valueArray = []
    const [arrayContent , setArrayContent] = useState([])
    const [arrayDisable , setArrayDisable] = useState([])
    const [array , setArray] = useState([])
    const [playerTurn , setPlayerTurn] = useState("Player: 1")
    const [player1 , setPlayer1] = useState(true)
    const [player2 , setPlayer2] = useState(false)
    const dim=5
    const orib1 = dim-1
    const orib2 = dim+1
    const width= (dim*50+50)
  
    

    useEffect(()=>{
        for (var i=0 ; i<dim*dim ; i++){
        valueArray.push({index:i , value:""})
        }
        setArray(valueArray)
  
    },[])

    const Board = styled.div`
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
     justify-content: center;
    align-items: center;
    align-content: center;
    gap: 0;
    margin: auto;
    width: ${props => props.width}px;
    margin-top: 150px;
    `;

    const square={
        border:"1px solid red",
        width:"50px",
        height:"50px",
        outline:"none",
        cursor:"pointer",
        textAlign:"center",
        lineHeight:"45px",
        fontSize:"40px"
    }
    const span={
       fontSize:"22px",
       display:"block",
       marginTop:"20px"
    }



    const handleClick=(e , id) =>{

        array[id].index = id

        if(player1 && array[id].value ==="") {
                setPlayerTurn("Player: 2")
                arrayContent[id] =  "X"
                arrayDisable[id] =  true
                array[id].value ="X"
                setPlayer1(false)
                setPlayer2(true)
                handleWinner("Player 1", id)

        }
        
        else if(player2 && array[id].value ==="") {
                setPlayerTurn("Player: 1")
                arrayContent[id] =  "O"
                arrayDisable[id] =  true
                array[id].value ="O"
                setPlayer2(false)
                setPlayer1(true)
                handleWinner("Player 2", id)  
        
    }
    }
    const handleWinner = (player , id) =>{
        if(array[id].value === array[id+1].value && array[id].value === array[id+2].value){
           alert(`Winner: ${player} `)
            handleReset()
        }
        else if(array[id].value === array[id-1].value && array[id].value === array[id+1].value ){
            alert(`Winner: ${player} `)
            handleReset()
        }
        else if(array[id].value === array[id-1].value && array[id].value === array[id-2].value){
            alert(`Winner: ${player} `)
            handleReset()
        }
        else if(array[id].value === array[id+dim].value && array[id].value === array[id+dim*2].value){
            alert(`Winner: ${player} `)
            handleReset()
        }
        else if(array[id].value === array[id-dim].value && array[id].value === array[id+dim].value){
            alert(`Winner: ${player} `)
            handleReset()
        }
        else if(array[id].value === array[id-dim].value && array[id].value === array[id-dim*2].value ){
            alert(`Winner: ${player} `)
            handleReset()
        }
        else if(array[id].value === array[id+orib2].value && array[id].value === array[id+orib2*2].value ){
            alert(`Winner: ${player} `)
            handleReset()
        }
        else if(array[id].value === array[id+orib2].value && array[id].value === array[id-orib2].value){
            alert(`Winner: ${player} `)
            handleReset()
        }
        else if(array[id].value === array[id-orib2].value && array[id].value === array[id-orib2*2].value ){
            alert(`Winner: ${player} `)
            handleReset()
        }
        else if(array[id].value === array[id+orib1].value && array[id].value === array[id+orib1+2].value ){
            alert(`Winner: ${player} `)
            handleReset()
        }
        else if(array[id].value === array[id+orib1].value && array[id].value === array[id-orib1].value){
            alert(`Winner: ${player} `)
            handleReset()
        }
        else if(array[id].value === array[id-orib1].value && array[id].value === array[id-orib1*2].value ){
            alert(`Winner: ${player} `)
            handleReset()
        }
        else{
            const spaceIndex = array.findIndex(item => item.value === "")
            if (spaceIndex===-1){
               alert("equal")
               handleReset()
            }
        }
    }
   
    const handleReset = () =>{
        setPlayerTurn("Player: 1")
        setPlayer1(true)
        setPlayer2(false)
        array.map(item => item.value = "")
        setArrayContent([])
        setArrayDisable([])
    }

    console.log(array)
    
    return(

        <Board width={width}>

        {array.map(value => 
                            <div key={value.index}
                                    onClick={e => handleClick(e , value.index)}
                                    style={square} 
                                    disabled={arrayDisable[value.index]}
                            >
                            {arrayContent[value.index]}
                            </div>
                            )
        }
        <div>
        <span style={span}>
            {playerTurn}
        </span>
        </div>

        </Board>
    )






}
export default App